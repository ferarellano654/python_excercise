#Las variables se declaran en camellcase

variable = True
print(variable)

#Las contantes se declaran en mayusculas

PI = 3.1416

print(PI)

"""
    Otra practica es declarar nuestras 
    constantes en otro archivo para poder usarlo 
    en el principal
"""